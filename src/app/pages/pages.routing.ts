import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { BingoCardComponent } from "./bingoCard/bingoCard.component";

const routes: Routes = [
    { 
        path: 'cartilla', 
        component: BingoCardComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class PagesRoutingModule {}