import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Auth } from '../interface/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private env = environment;
  protected endpoint: string;

  constructor(
    private http: HttpClient,
  ) {
    this.endpoint = `${environment.api}/v1`;
  }

  postLogin(auth: any){
    return this.http.post(`${this.endpoint}/user/signin`, auth);
  }
  
}
